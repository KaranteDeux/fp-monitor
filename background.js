const pageIDToScriptsID = new Map();
const pageIDToWebpage = new Map();
const scriptPageToTraces = new Map();


function buildKey(scriptID, pageID) {
  return `${pageID}, ${scriptID}`;
}

const defaultExceptions = {
  'domains': [
      urldomain('https://docs.google.com'),
      urldomain('https://drive.google.com'),
      urldomain('https://zimbra.inria.fr')
  ],
  'urls' : []
};

let tags = [];

async function storageGet(item){
  return new Promise(function(resolve, reject) {
    chrome.storage.local.get(item, function(elements){
        resolve(elements);
    });
  });
}

function storageSet(key, item){
  return new Promise(function(resolve, reject){
    chrome.storage.local.set({key: item}, function(){
      resolve('OK');
    });
  })
}

async function get_exceptions(){
  let exceptions = await storageGet('exceptions');
  if(exceptions === undefined || Object.keys(exceptions).length === 0){
    exceptions = defaultExceptions;
    await storageSet('exceptions', exceptions);
  }

  return exceptions;
}
function urldomain(data) {
  const regex = RegExp('^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)');
  let array1 = data.match(regex);
  if(array1.length) {
    return array1[0];
  }
  return ""
}

async function addException(request) {
  return new Promise(async function(resolve, reject) {
    const exceptions = await get_exceptions();
    if (request.type === 'domain') {
      const newDomainException = urldomain(request.url);
      if (!exceptions.domains.includes(newDomainException)) {
        exceptions.domains.push(newDomainException);
      }
    } else if (request.type === 'page') {
      if (!exceptions.urls.includes(request.url)) {
        exceptions.urls.push(request.url);
      }
    }

    await storageSet('exceptions', exceptions);
    resolve({state: "OK"});
  });
}

function getException(request){
  return new Promise(async function(resolve, reject){

    const exceptionsFound = {
      page: false,
      domain: false,
    };
    const exceptions = await get_exceptions();

    console.log(exceptions);

    if(exceptions.urls.includes(request.url)){
      exceptionsFound.page = true;
    }
    let domain = urldomain(request.url);
    if(exceptions.domains.includes(domain)){
      exceptionsFound.domain = true;
    }
    resolve({state: "OK", exceptions: exceptionsFound});
  })

}

async function removeException(request){
  return new Promise(async function(resolve, reject) {

    const exceptions = await get_exceptions();
    if (request.type === 'domain') {
      const domainToRemove = urldomain(request.url);
      const index = exceptions.domains.indexOf(domainToRemove);
      if (index >= 0) {
        exceptions.domains.splice(index, 1);
      }
    } else if (request.type === 'page') {
      const index = exceptions.urls.indexOf(request.url);
      if (index >= 0) {
        exceptions.urls.splice(index, 1);
      }
    }
    chrome.storage.local.set({'exceptions': exceptions});
    resolve({state: "OK"});
  });
}

const URL = "http://localhost:8090/store";
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if(request.goal !== undefined && request.goal === 'addException') {
    addException(request).then(function(response){
      sendResponse(response);
    });
  } else if(request.goal === 'getException') {
    getException(request).then(function(response){
      sendResponse(response);
    });
  } else if(request.goal === 'removeException') {
    removeException(request).then(function(response){
      sendResponse(response);
    });
  } else if(request.goal === 'attributeAccessed') {
    chrome.runtime.sendMessage({
      goal: "forPopupAttributeAccessed",
      data: request.data
    });
  } else if(request.goal === 'getTags') {
    sendResponse({tags:tags});
  } else if(request.goal === 'sendTags') {
    tags = request.tags;
    chrome.tabs.query({
        active: true,
        currentWindow: true
      }, function(tabs) {
        const tab = tabs[0];
        const url = tab.url;
        chrome.tabs.sendMessage(tabs[0].id, {
            goal: "forContentScriptSendData",
            tags: request.tags,
            targetPhishing: request.targetPhishing,
            url: url,
          },
          function(response) {
            if(response.status === 'Data sent')
            sendResponse({'status': 'Data sent'})
        });
    });
  } else if(request.goal === 'sendData'){
    console.log('sending request...');
    const req = new XMLHttpRequest();
    req.onreadystatechange = function() {
      if (req.readyState === 4) {
        console.log(req.response); // Par défault une DOMString
      }
    };
    req.open('POST', URL, true);
    req.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    req.send(JSON.stringify(request.data));
    console.log('Data should be stored');

    sendResponse({'status': 'OK'});
  }

  return true;
});
