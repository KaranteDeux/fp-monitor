function injectScript(text){
	const parent = document.documentElement;
	const script = document.createElement("script");

	script.text = text;
	script.async = false;
	parent.insertBefore(script, parent.firstChild);
	parent.removeChild(script);
}

function generatePageId() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4();
}

const PAGE_ID = generatePageId();

let datas = [];
dataSent = false;

function getDate(){
    const today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;

    const yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return dd + '/' + mm + '/' + yyyy;
}


// chrome.runtime.sendMessage(datas, function(response) {});

function arrayUnique(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
}

function sendDataToPopup(){
    chrome.runtime.sendMessage({
        goal: "attributeAccessed",
        data: datas,
    });
    setTimeout(sendDataToPopup, 2000);
}
setTimeout(sendDataToPopup, 2000);

function sendData(/* tags */){
    if(Array.isArray(datas) && datas.length) {
        if(fonts.length) {
            datas = datas.concat(fonts);
        }

        if(renderingContextElements.length) {
            datas = datas.concat(renderingContextElements);
        }
        chrome.runtime.sendMessage({goal:'sendData', data: datas}, function(response) {});
        dataSent = true;
    }
}

// After 30 seconds, and if the data were not sent yet, we sent them
let fonts = [];


function updateFonts(f) {
    f.arguments = f.arguments.split(',');
    const cleanedFonts = [];
    f.arguments.forEach(function(arg){
        cleanedFonts.push(arg.trim())
    });

    f.arguments = cleanedFonts;
    let foundI = -1;
    for(let i in fonts){
        const font = fonts[i];
        if(font.pageID === f.pageID && font.origin === f.origin) {
            foundI = i;
        }
    }

    if(foundI === -1){
        fonts.push(f);
    } else {
        fonts[foundI].arguments = arrayUnique(fonts[foundI].arguments.concat(f.arguments));
    }
    document.getElementById('toto').innerText = JSON.stringify(datas.concat(fonts).concat(renderingContextElements));
}

let renderingContextElements = [];
const renderingElementsLimit = 100;

function updateCanvasRenderingContext2D(e) {
    let foundI = -1;
    for(let i in renderingContextElements){
        const element = renderingContextElements[i];
        if(element.thisToString === e.thisToString && element.pageID === e.pageID && element.origin === e.origin) {
            foundI = i;
        }
    }
    const splitted = e.property.split('\.');
    e.property = splitted[splitted.length-1];

    if(foundI === -1){
        e.elements = [];
        e.elements.push({
            property: e.property,
            arguments: e.arguments,
            value: e.value
        });

        delete e.property;
        delete e.arguments;
        delete e.value;

        renderingContextElements.push(e);
    } else {
        if(renderingContextElements[foundI].elements.length >= renderingElementsLimit) {
            let initialLength;
            if('initialLength' in renderingContextElements[foundI]) {
                initialLength = renderingContextElements[foundI].initialLength + 1;
            } else {
                initialLength = renderingElementsLimit + 1;
            }

            renderingContextElements[foundI].initialLength = initialLength;
            renderingContextElements[foundI].message = "Context calls were cut. " + renderingElementsLimit + " elements were kept, out of " + initialLength;
        } else {
            renderingContextElements[foundI].elements.push({
                property: e.property,
                arguments: e.arguments,
                value: e.value
            });
        }
    }
    document.getElementById('toto').innerText = JSON.stringify(datas.concat(fonts).concat(renderingContextElements));
}

function isEquivalent(a, b) {
    if(typeof a !== typeof b) {
        return false;
    }

    if(typeof a !== 'object') {
        return a === b;
    }

    if(a === null || a === undefined || b === null || b === undefined){
        return false;
    }
    // Create arrays of property names
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);

    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length !== bProps.length) {
        return false;
    }

    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];

        // If values of same property are not equal,
        // objects are not equivalent
        if(!isEquivalent(a[propName], b[propName])) {
            return false;
        }
    }

    // If we made it this far, objects
    // are considered equivalent
    return true;
}

// Need to keep N first elements of analyserNode (otherwise it's way too big)
function firstN(obj, n) {
    return Object.keys(obj) //get the keys out
        .sort() //this will ensure consistent ordering of what you will get back. If you want something in non-aphabetical order, you will need to supply a custom sorting function
        .slice(0, n) //get the first N
        .reduce(function(memo, current) { //generate a new object out of them
            memo[current] = obj[current];
            return memo;
        }, {})
}

const cutAnalyserNodeParameter = 30;

const mergeThisToStringProperty = function(thisToString, property){
    let str = property;
    if(property !== undefined && thisToString !== undefined && !property.includes(thisToString) && !thisToString.includes(property)){
        str = thisToString + property;
    }
    return str;
};

const thisToStringProperties = [];


function updateDatas(toBeAddedData) {
    toBeAddedData.nbTimes = 1;

    if (toBeAddedData.thisToString === '[object AnalyserNodePrototype]' && toBeAddedData.property === 'AnalyserNode.prototype.getFloatFrequencyData') {
        const initialLength = Object.keys(toBeAddedData.arguments[0]).length;
        toBeAddedData.arguments[0] = firstN(toBeAddedData.arguments[0], cutAnalyserNodeParameter);
        toBeAddedData.message = "Argument was cut. " + cutAnalyserNodeParameter + " elements were kept, out of " + initialLength;
        toBeAddedData.initialLength = initialLength;
    }

    // Check if the element we want to add has already been accessed, with same parameters and same results
    let mustAdd = true;
    datas.forEach(function (data) {
        let similar = true;
        const keys = Object.keys(data);
        const toBeAddedKeys = Object.keys(toBeAddedData);
        if (keys.length !== toBeAddedKeys.length) {
            similar = false
        } else {
            keys.forEach(function (key) {
                if (key !== "nbTimes" && !isEquivalent(data[key], toBeAddedData[key])) {
                    // If one value in the map is different, the object is different
                    similar = false;
                }
            });
        }
        // If object is similar, we increment its nbTimes, and don't add the new one
        if (similar) {
            data.nbTimes += 1;
            mustAdd = false;
        }
    });

    const thisToStringProperty = mergeThisToStringProperty(toBeAddedData.thisToString, toBeAddedData.property);
    if (!thisToStringProperties.includes(thisToStringProperty)) {
        thisToStringProperties.push(thisToStringProperty);
        console.log(thisToStringProperties);
    }

    // If we add, we add (dat obvious comment)
    if (mustAdd) {
        datas.push(toBeAddedData);
    }

    document.getElementById('toto').innerText = JSON.stringify(datas.concat(fonts).concat(renderingContextElements));

}

function updateElementAndSend(tags, url, targetPhishing){
    if(document.location.href === url){
        if(!datas.length){
            datas.push({
                thisToString: 'NO FINGERPRINTING',
                property: 'NO FINGERPRINTING',
                arguments: '',
                value: 'NO FINGERPRINTING',
                origin: 'NO FINGERPRINTING',
                webpage: window.location.href,
                pageID: PAGE_ID,
                date: getDate(),
            });
        }

        for(const i in datas){
            const data = datas[i];
            if(data.webpage === url){
                data.tags = tags;
                console.log('targetPhishing : ' + targetPhishing);
                if(targetPhishing !== ''){
                    data.targetPhishing = targetPhishing;
                }
                // Check if tag is phishing, fraudulent or illegal)
                for(const i in data.tags){
                    if (data.tags[i] === 'website-type-phishing' ||
                      data.tags[i] === 'website-type-illegal' ||
                      data.tags[i] === 'website-type-fraudulent'){
                        data.pageContent = document.documentElement.outerHTML;
                    }
                }

                sendData(/* tags */);
                return {'status': 'Data sent'};
            }
        }
        return {'status':'Cannot find data with same URL'};
    }
    return {'status':'Houston, we have a problem'};

}


chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    console.log('in content script');
    if(request.goal === 'forContentScriptSendData'){
        const res = updateElementAndSend(request.tags, request.url, request.targetPhishing);
        if(typeof res === 'object'){
            sendResponse(res);
        }
    }
});

window.addEventListener("message", (event) => {
	// We only accept messages from ourselves
    if (event.source !== window)
		return;

    if(event.msg === "attributeAccessed"){
        return;
    }


	const renderingStuff = ['[object CanvasRenderingContext2DPrototype]',
                            '[object WebGLRenderingContextPrototype]',
                            '[object WebGL2RenderingContextPrototype]'];

    let data;
    try {
        data = JSON.parse(event.data);
    } catch(e){
        return;
    }

    // We also do not want stuff from debugger
	  if('webpage' in data && data.webpage !== "undefined"){

      if(data.property === "font.enumerate") {
          updateFonts(data);
      } else if(renderingStuff.includes(data.thisToString)){
          updateCanvasRenderingContext2D(data);
      } else {
          updateDatas(data);
      }
	  }
});

function fingerprintingChanges(){

    const attributesToMonitor = {
        navigator: {
            'userAgent': "Mozilla/5.0 (Unknown; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) PhantomJS/1.9.8 Safari/537.36",
            'plugins': [],
            'mimeTypes': {},
    /*
            'doNotTrack',
            'languages': "",
            'productSub',
            'language',
            'oscpu',
            'hardwareConcurrency',
            'cpuClass',
      */
            'webdriver':true,
            'chrome': undefined,
        /*
            'vendor',
            'vendorSub',
            'buildID',
        */
        },
        screen: {
            'width': 640,
            'height': 480,
            'availWidth': 640,
            'availHeight': 480,
        },
        window: {
            'ActiveXObject': {},
            'webdriver': {},
            'domAutomation': {},
            'domAutomationController': {},
            'callPhantom': {},
            'spawn': {},
            'emit': {},
            'Buffer': {},
            'awesomium': {},
            '_Selenium_IDE_Recorder': {},
            '__webdriver_script_fn': {},
            '_phantom': {},
            'callSelenium': {},
            '_selenium': {},
        }

    };
    for (let prop of Object.keys(attributesToMonitor)) {
        for (let subProp of Object.keys(attributesToMonitor[prop])) {
            if(prop !== 'window') {
                window[prop].__defineGetter__(subProp, () => {
                    return attributesToMonitor[prop][subProp];
                });
            } else {
                window.__defineGetter__(subProp, () => {
                    return attributesToMonitor[prop][subProp];
                });
            }
        }
    }
}

function fingerprintingDetection() {
    let dataArray = [];

    function getDate(){
        const today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;

        const yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return dd + '/' + mm + '/' + yyyy;
    }


    const undesirableDomains = ['https://docs.google.com'];
    undesirableDomains.forEach(function(undesirableDomain){
        if(document.URL.startsWith(undesirableDomain)) {
            return;
        }
    });

    function sendMessage(thisToString, property, value, args) {
        const origin = getCallerFile();

        const data = {
            thisToString: thisToString,
            property: property,
            arguments: args.length > 0 ? args : '',
            value: typeof value === 'object' ? 'object' : value,
            origin: origin,
            webpage: window.location.href,
            pageID: PAGE_ID,
            date: getDate()
        };

        dataArray.push(data);

        window.postMessage(JSON.stringify(data), "*");
    }

    function getFirefoxFilename(call){
        let regex = RegExp('https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b[-a-zA-Z0-9()@%_\+.~#?&\/=]*', 'g');
        let array1 = call.match(regex);
        if(array1.length) {
            return array1[0];
        }
        return ""
    }

    function getCallerFile() {
        let originalFunc = Error.prepareStackTrace;

        let callerfile = undefined;
        try {
            const err = new Error();
            let currentfile;

            Error.prepareStackTrace = function (err, stack) {
                return stack;
            };

            if(typeof err.stack === "string") {
                // Code for Firefox
                const stacktraceArray = err.stack.split('\n');
                stacktraceArray.forEach(function(call){
                    const callerfileN = getFirefoxFilename(call);
                    if(currentfile === undefined){
                        currentfile = callerfileN;
                    }

                    if(callerfile === undefined && currentfile !== callerfileN){
                        callerfile = callerfileN;
                    }
                });

            } else {
                // Code for chrome
                currentfile = err.stack.shift().getFileName();

                while (err.stack.length) {
                    callerfile = err.stack.shift().getFileName();
                    if (currentfile !== callerfile) break;
                }
            }
        } catch (e) {
        }

        Error.prepareStackTrace = originalFunc;

        return callerfile;
    }

    function overrideFunction(item) {
        item.obj[item.propName] = (function (orig) {
            return function () {

                const args = arguments;
                let thisToString;
                try {
                    if (item.objName === 'Date.prototype') {
                        thisToString = item.objName;
                    } else {
                        thisToString = item.obj.toString();
                    }
                } catch(e){
                    thisToString = item.objName
                }


                sendMessage(thisToString, `${item.objName}.${item.propName}`, orig.apply(this, args), args);

                return orig.apply(this, args);
            };
        }(item.obj[item.propName]));
    }

    const attributesToMonitor = {
        navigator: [
            'userAgent',
            'platform',
            'plugins',
            'mimeTypes',
            'doNotTrack',
            'languages',
            'productSub',
            'language',
            'oscpu',
            'hardwareConcurrency',
            'cpuClass',
            'webdriver',
            'chrome',
            'vendor',
            'vendorSub',
            'buildID'
        ],
        screen: [
            'width',
            'height',
            'availWidth',
            'availHeight',
            'availTop',
            'availLeft',
            'colorDepth',
            'pixelDepth'
        ],
        window: [
             'ActiveXObject',
             'webdriver',
             'domAutomation',
             'domAutomationController',
             'callPhantom',
             'spawn',
             'emit',
             'Buffer',
             'awesomium',
             '_Selenium_IDE_Recorder',
             '__webdriver_script_fn',
             '_phantom',
             'callSelenium',
             '_selenium',
         ]
    };

    window.navigator.monitorFingerprinting = {};
    const originalValues = {};

    for (let prop of Object.keys(attributesToMonitor)) {
        for (let subProp of attributesToMonitor[prop]) {
            if(prop !== 'window') {
                originalValues[subProp] = window[prop][subProp];
                window[prop].__defineGetter__(subProp, () => {
                    sendMessage(`window.${prop}.${subProp}`, `window.${prop}.${subProp}`, JSON.stringify(originalValues[subProp]), '');
                    return originalValues[subProp];
                });
            } else {
                originalValues[subProp] = window[subProp];
                window.__defineGetter__(subProp, () => {
                    sendMessage(`window.${subProp}`, `window.${subProp}`, JSON.stringify(originalValues[subProp]), '');
                    return originalValues[subProp];
                });
            }
        }
    }

    const audioContextMethods = ['createAnalyser', 'createOscillator', 'createGain',
        'createScriptProcessor', 'createDynamicsCompressor'];
    audioContextMethods.forEach((method) => {
        overrideFunction({
            objName: 'AudioContext.prototype',
            propName: method,
            obj: window.AudioContext.prototype
        });
    });

     const audioBuffMethods = ['copyFromChannel', 'getChannelData'];
     audioBuffMethods.forEach((method) => {
         overrideFunction({
             objName: 'AudioBuffer.prototype',
             propName: method,
             obj: window.AudioBuffer.prototype
         });
     });

    const analyserMethods = ['getFloatFrequencyData', 'getByteFrequencyData',
        'getFloatTimeDomainData', 'getByteTimeDomainData'];
    analyserMethods.forEach((method) => {
        overrideFunction({
            objName: 'AnalyserNode.prototype',
            propName: method,
            obj: window.AnalyserNode.prototype
        });
    });

    const webGLMethods = ['getParameter', 'getSupportedExtensions', 'getContextAttributes',
        'getShaderPrecisionFormat', 'getExtension', 'readPixels', 'getUniformLocation',
        'getAttribLocation'];
    webGLMethods.forEach((method) => {
        overrideFunction({
            objName: 'WebGLRenderingContext.prototype',
            propName: method,
            obj: window.WebGLRenderingContext.prototype
        });
    });

    const canvasEltMethods = ['getContext', 'toDataURL', 'toBlob'];
    canvasEltMethods.forEach((method) => {
        overrideFunction({
            objName: 'HTMLCanvasElement.prototype',
            propName: method,
            obj: HTMLCanvasElement.prototype
        });
    });


// handler used for intercepting proxy
    const handler = {
        get (target, key) {
            // forward access to underlying property
            const value = Reflect.get(target, key)
            // return underlying value as an intercepting proxy
            // if value is function, its calling context will be the target
            return proxyValue(target, value)
        },
        set (target, key, value) {
            // forward access to underlying property
            Reflect.set(target, key, value)
            // return set value as an intercepting proxy
            // if value is function, it will not have a calling context
            return proxyValue(undefined, value)
        },
        apply (target, thisArg, args) {
            // forward invocation to underlying function
            const value = Reflect.apply(target, thisArg, args)
            // return underlying return value as an intercepting proxy
            // if value is a function, it will not have a calling context
            return proxyValue(undefined, value)
        }
    }

// wrap all accessed non-primitives in an intercepting proxy
    function proxyValue (target, value) {
        switch (typeof value) {
            case 'function':
                // return function bound to target as proxy
                return new Proxy(value.bind(target), handler)
            case 'object':
                // return object as proxy
                return new Proxy(value, handler)
            default:
                // return primitive as normal
                return value
        }
    }

// monitor every property/functions
    const objs = [CanvasRenderingContext2D.prototype, WebGLRenderingContext.prototype, WebGL2RenderingContext.prototype]

    const keyNotToAdd = ['transform', 'translate', 'rotate'];
    objs.forEach(function(obj) {
        const descriptors = Object.getOwnPropertyDescriptors(obj);

        Object.entries(descriptors).forEach(([key, {value, get, set, configurable, enumerable, writable}]) => {
            // if this is an accessor property

            if (get || set) {
                const accessor = {
                    get() {
                        // forward access to underlying getter
                        const getValue = Reflect.apply(get, this, [])
                        sendMessage(obj.toString(), key, getValue, '');

                        // return underlying value as an intercepting proxy
                        return proxyValue(this, getValue)
                    },
                    set(setValue) {
                        // forward access to underlying setter
                        Reflect.apply(set, this, [setValue]);
                        sendMessage(obj.toString(), key, undefined, setValue);

                        // return set value as an intercepting proxy
                        return proxyValue(this, setValue)
                    },
                    configurable,
                    enumerable,
                };

                // overwrite property with intercepting accessor property
                Object.defineProperty(obj, key, accessor)

            } else if(typeof obj[key] === 'function'){
                if(obj !== CanvasRenderingContext2D.prototype && !(keyNotToAdd.includes(key))) {
                    overrideFunction({
                        objName: obj.toString(),
                        propName: key,
                        obj: obj
                    });
                }
            }
        });
    });

    const webrtcMethods = ['createOffer', 'createAnswer', 'setLocalDescription', 'setRemoteDescription'];
    if(typeof webkitRTCPeerConnection !== "undefined")
    webrtcMethods.forEach((method) => {
        overrideFunction({
            objName: 'webkitRTCPeerConnection.prototype',
            propName: method,
            obj: webkitRTCPeerConnection.prototype
        });
    });

    const mediaMethods = ['canPlayType'];
    mediaMethods.forEach((method) => {
        overrideFunction({
            objName: 'HTMLMediaElement.prototype',
            propName: method,
            obj: HTMLMediaElement.prototype
        });
    });


    const otherFunctionsToOverride = [
        {
            objName: 'Date.prototype',
            propName: 'getTimezoneOffset',
            obj: Date.prototype
        },
        {
            objName: 'SVGTextContentElement.prototype',
            propName: 'getComputedTextLength',
            obj: SVGTextContentElement.prototype
        },
        {
            objName: 'navigator.permissions',
            propName: 'query',
            obj: navigator.permissions
        },
    ];
    try {

        otherFunctionsToOverride.forEach(overrideFunction);

        const originalValueKeyboardLayoutMap = 'keyboard.getLayoutMap()';
        const keyboardGetLayoutMapToString = 'window.navigator.keyboard.getLayoutMap()';

        // Keyboard
        if (!('keyboard' in window['navigator']))
            window['navigator'].keyboard = {};

        if (!(originalValueKeyboardLayoutMap in originalValues)) {
            if (typeof window['navigator']['keyboard'].getLayoutMap === "function")
                originalValues[originalValueKeyboardLayoutMap] = window['navigator']['keyboard'].getLayoutMap();
            else
                originalValues[originalValueKeyboardLayoutMap] = undefined;
        }

        window['navigator']['keyboard'].getLayoutMap = function () {
            const result = originalValues[originalValueKeyboardLayoutMap];
            if (typeof result === "object") {
                result.then(function (keyboard) {
                    let keyboard_result = {};
                    keyboard.forEach(function (key, v) {
                        keyboard_result[v] = key;
                    });
                    sendMessage(keyboardGetLayoutMapToString, keyboardGetLayoutMapToString, keyboard_result, '');
                });
            } else {
                sendMessage(keyboardGetLayoutMapToString, keyboardGetLayoutMapToString, result, '');
            }

            return result;
        };

        // Media devices
        const originalValueMediaDevicesEnumerate = 'mediaDevices.enumerateDevices()';
        const mediaDevicesEnumerateToString = 'window.navigator.mediaDevices.enumerateDevices()';

        if (!('mediaDevices' in window['navigator']))
            window['navigator'].mediaDevices = {};

        if (!(originalValueMediaDevicesEnumerate in originalValues)) {
            if (typeof window['navigator']['mediaDevices'].enumerateDevices === "function") {
                originalValues[originalValueMediaDevicesEnumerate] = window['navigator']['mediaDevices'].enumerateDevices();
            } else {
                originalValues[originalValueMediaDevicesEnumerate] = undefined;
            }
        }

        window['navigator']['mediaDevices'].enumerateDevices = function () {
            const result = originalValues[originalValueMediaDevicesEnumerate];
            sendMessage(mediaDevicesEnumerateToString, mediaDevicesEnumerateToString, result, '');

            return result;
        };

        // Font enumeration
        const propertiesName = ['offsetWidth', 'offsetHeight', 'clientWidth', 'clientHeight'];
        const objNames = [HTMLSpanElement.prototype];

        propertiesName.forEach(function (propName) {
            objNames.forEach(function (obj) {
                Object.defineProperty(obj, propName, {
                    'get': function () {
                        const fontFamily = this.style.fontFamily;
                        if (fontFamily !== "") {
                            sendMessage(this.toString(), `font.enumerate`, obj.propName, fontFamily);
                        }
                        return obj.propName
                    }
                })
            })
        });

        // Battery
        if (typeof window['navigator'].getBattery === "function")
            originalValues['navigator.getBattery()'] = window['navigator'].getBattery();
        else
            originalValues['navigator.getBattery()'] = undefined;

        window['navigator'].getBattery = function () {
            const result = originalValues['navigator.getBattery()'];
            if (typeof result === "object") {
                result.then(function (b) {
                    const battery = {};
                    battery['charging'] = b.charging;
                    battery['chargingTime'] = b.chargingTime;
                    battery['dischargingTime'] = b.charging;
                    battery['level'] = b.level;

                    sendMessage(`window.navigator.getBattery()`, `window.navigator.getBattery()`, battery, '');
                });
            }

            return result;
        };

        // Connection
        if (typeof window['navigator'].connection === "function")
            originalValues['navigator.connection'] = window['navigator'].connection;
        else
            originalValues['navigator.connection'] = undefined;

        window['navigator'].connection = function () {
            const result = originalValues['navigator.connection'];
            if (typeof result === "object") {
                const connection = {};
                connection['downlink'] = result.downlink;
                connection['effectiveType'] = result.effectiveType;
                connection['rtt'] = result.rtt;
                connection['saveData'] = result.saveData;

                sendMessage(`window.navigator.connection`, `window.navigator.connection`, connection, '');
            }

            return result;
        };

        // Storage
        const originalValueStorageEstimate = 'storage.estimate()';
        const storageEstimateToString = 'window.navigator.storage.estimate()';

        if (!('storage' in window['navigator']))
            window['navigator'].storage = {};

        if (!(originalValueStorageEstimate in originalValues)) {
            if (typeof window['navigator']['storage'].estimate === "function")
                originalValues[originalValueStorageEstimate] = window['navigator']['storage'].estimate();
            else
                originalValues[originalValueStorageEstimate] = undefined;
        }

        window['navigator'].storage.estimate = function () {
            const result = originalValues[originalValueStorageEstimate];
            result.then(function (estimation) {
                sendMessage(storageEstimateToString, storageEstimateToString, estimation, '');
            });

            return result
        };
    } catch(e){
        console.log('FP Extension Monitor : Error here')
    }
}

// Check if the page or domain has been whitelisted by the user
chrome.runtime.sendMessage({
    goal: "getException",
    url: document.URL
}, function(response){
    if(response.state === "OK") {
        if(!response.exceptions.domain && !response.exceptions.page) {
            injectScript(`
                const PAGE_ID = '${PAGE_ID}';
                ${fingerprintingDetection.toString()};
                fingerprintingDetection();
            `);
        } else {
            console.log('FP Extension monitor - not loaded');
        }
    }
});