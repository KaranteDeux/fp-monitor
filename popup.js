document.addEventListener('DOMContentLoaded', async function() {


    async function getCurrentURL(){
        return new Promise(function(resolve, reject){
            chrome.tabs.query({currentWindow: true, active: true}, function(tabs){
                resolve(tabs[0].url);
            });
        });
    }

    const url = await getCurrentURL()


    function sendExceptionModificationQuery(goal, type){
        return new Promise(function(resolve, reject){
            chrome.runtime.sendMessage({
                goal: goal,
                type: type,
                url: url
            }, function(response){
                if(response.state === "OK") {
                    resolve("OK")
                } else {
                    resolve("FAIL")
                }
            });
        });
    }

    function updateToggles(){
        chrome.runtime.sendMessage({
            goal: "getException",
            url: url
        }, function(response){
            if(response.state === "OK") {
                if(response.exceptions.domain) {
                    document.getElementById("toggle-domain").checked = true;
                }

                if(response.exceptions.page) {
                    document.getElementById("toggle-page").checked = true;
                }
            }
        });
    }

    updateToggles();

    let tags = [];
    const tagButtons = document.getElementsByClassName("tag");
    for (i = 0; i < tagButtons.length; i++) {
        tagButtons[i].addEventListener('click', async function(event) {

            const tagPrefix = 'tag-';
            let tag = "";
            this.classList.forEach(function(c){
                if(c.includes(tagPrefix)){
                    tag = c.replace(tagPrefix, '');
                }
            });
            if(!tags.includes(tag)){
                tags.push(tag);
                this.classList.remove("btn-secondary")
                this.classList.add("btn-success")
            } else {
                tags.splice( tags.indexOf(tag), 1 );
                this.classList.remove("btn-success");
                this.classList.add("btn-secondary");
            }

            console.log(tags);
        });
    }

    chrome.runtime.sendMessage({
        goal: "getTags",
    }, function(response) {
        tags = response.tags;
        console.log('tags : ' + tags);
        for (i = 0; i < response.tags.length; i++) {
            console.log(response.tags[i]);
            // tags.splice( tags.indexOf(tag), 1 );
            const element = document.getElementsByClassName('tag-' + response.tags[i])[0];

            element.classList.remove("btn-secondary")
            element.classList.add("btn-success")
        }
    });

    document.getElementById('validate').addEventListener('click', function(){
        const objToSend = {
            goal: "sendTags",
            tags: tags,
            targetPhishing: '',
        };
        for(const i in tags){
            if (tags[i] === 'website-type-phishing') {
                objToSend.targetPhishing = document.getElementById('target-phishing').value
            }
        }
        chrome.runtime.sendMessage(objToSend, function(response) {
            if(response !== undefined && response !== null && Object.keys(response).includes('status')){
                document.getElementById('message').innerText = response.status;
            }
        });
        document.getElementById('message').innerText = 'Should be ok';
    });

    /*
    document.getElementById("toggle-page").addEventListener('click', async function(event){
        let goal;
        if(this.checked){
            goal = 'addException';
        } else {
            goal = 'removeException';
        }

        const res = await sendExceptionModificationQuery(goal, 'page');
        if(res === "OK") {
            if(this.checked) {
                document.getElementById('message').innerText = "Page added to the exception list";
            } else {
                document.getElementById('message').innerText = "Page removed from the exception list";
            }
        }
    });

    document.getElementById("toggle-domain").addEventListener('click', async function(event){
        let goal;
        if(this.checked){
            goal = 'addException';
        } else {
            goal = 'removeException';
        }

        const res = await sendExceptionModificationQuery(goal, 'domain');
        if(res === "OK") {
            if(this.checked) {
                document.getElementById('message').innerText = "Domain added to the exception list";
            } else {
                document.getElementById('message').innerText = "Domain removed from the exception list";
            }
        }
    });
    */

    let datas = [];
    let rearrangedData = {};
    chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
        if(request.goal === 'forPopupAttributeAccessed'){
            datas = request.data;
            if(datas.length) {
                datas.forEach(function (data) {
                    if (!Object.keys(rearrangedData).includes(data.origin)) {
                        rearrangedData[data.origin] = {};
                    }
/*
                    if (Object.keys(rearrangedData[data.origin]).includes(data.property)) {
                        rearrangedData[data.origin][data.property] = rearrangedData[data.origin][data.property] + data.nbTimes;
                    } else {
                    */
                        rearrangedData[data.origin][data.property] = data.nbTimes;
//                    }
                })
            }
            var fingerprintersTable = document.getElementById('fingerprinters');
            var fingerprintersSortedTable = document.getElementById('fingerprintersSorted');

            var child = fingerprintersTable.lastElementChild;
            while (child) {
                fingerprintersTable.removeChild(child);
                child = fingerprintersTable.lastElementChild;
            }

            var child = fingerprintersSortedTable.lastElementChild;
            while (child) {
                fingerprintersSortedTable.removeChild(child);
                child = fingerprintersSortedTable.lastElementChild;
            }

            createEmptySortedFingerprinterTable(fingerprintersSortedTable);
            createEmptyFingerprinterTable(fingerprintersTable);

            fillFingerprinterTable(fingerprintersTable, rearrangedData);
            fillSortedFingerprinterTable(fingerprintersSortedTable, rearrangedData);
        }
    });

    function createEmptySortedFingerprinterTable(sortedFingerprintersTable) {
        var tr = document.createElement("tr");
        var th1 = document.createElement("th");
        var th2 = document.createElement("th");
        var th3 = document.createElement("th");
        var domain = document.createTextNode("Domain");
        var script = document.createTextNode("Script");
        var nbProperties = document.createTextNode("Number of different properties accessed");

        th1.appendChild(domain);
        th2.appendChild(script);
        th3.appendChild(nbProperties);
        tr.appendChild(th1);
        tr.appendChild(th2);
        tr.appendChild(th3);
        sortedFingerprintersTable.appendChild(tr);
    }
    function createEmptyFingerprinterTable(fingerprintersTable) {
        var tr = document.createElement("tr");
        var th1 = document.createElement("th");
        var th2 = document.createElement("th");
        var th3 = document.createElement("th");
        var script = document.createTextNode("Script");
        var property = document.createTextNode("Property");
        var numberAccess = document.createTextNode("Number of accesses");

        th1.appendChild(script);
        th2.appendChild(property);
        th3.appendChild(numberAccess);
        tr.appendChild(th1);
        tr.appendChild(th2);
        tr.appendChild(th3);
        fingerprintersTable.appendChild(tr);
    }

    function fillSortedFingerprinterTable(sortedFingerprinterTable, rearrangedData) {
	console.log('data : ');
	console.log(rearrangedData);
        const fingerprinters = Object.keys(rearrangedData);
        const domainRegex = /^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)/g;
        fingerprinters.forEach(function(fingerprinter) {
            let domainName = domainRegex.exec(fingerprinter);
            if(domainName !== null && domainName !== undefined){
                domainName = domainName[1];
                let fileName = fingerprinter.substring(fingerprinter.lastIndexOf('/') + 1);
                const indexOf = fileName.indexOf('?');
                if(indexOf === -1){
                    fileName = fileName.substring(0);
                } else {
                    fileName = fileName.substring(0, indexOf);
                }

                var tr = document.createElement("tr");
                var td1 = document.createElement("td");
                var td2 = document.createElement("td");
                var td3 = document.createElement("td");
                td1.innerText = domainName;
                td2.innerText = fileName;
                td3.innerText = Object.keys(rearrangedData[fingerprinter]).length + "";
                tr.appendChild(td1);
                tr.appendChild(td2);
                tr.appendChild(td3);

                sortedFingerprinterTable.appendChild(tr);
            }
        });
    }

    function fillFingerprinterTable(fingerprintersTable, rearrangedData){
        const fingerprinters = Object.keys(rearrangedData);
        const domainRegex = /^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)/g;
        fingerprinters.forEach(function(fingerprinter) {
            const properties = Object.keys(rearrangedData[fingerprinter]);
            properties.forEach(function(property) {

                let domainName = domainRegex.exec(fingerprinter);
                if(domainName !== null && domainName !== undefined) {
                    domainName = domainName[1];
                    const fileName = fingerprinter.substring(fingerprinter.lastIndexOf('/') + 1);

                    var tr = document.createElement("tr");
                    var td1 = document.createElement("td");
                    var td2 = document.createElement("td");
                    var td3 = document.createElement("td");
                    td1.innerText = domainName + "/<something>/ " + fileName;
                    td2.innerText = property;
                    td3.innerText = rearrangedData[fingerprinter][property];
                    tr.appendChild(td1);
                    tr.appendChild(td2);
                    tr.appendChild(td3);

                    fingerprintersTable.appendChild(tr);
                }
            });
        });
    }

    function sortTable(table, columnInd) {
        var rows, switching, i, x, y, shouldSwitch;
        switching = true;
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[columnInd];
                y = rows[i + 1].getElementsByTagName("TD")[columnInd];
                // Check if the two rows should switch place:
                if (parseInt(x.innerHTML) > parseInt(y.innerHTML)) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }
    }

    chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
        datas = [];
        rearrangedData = {};
    });
    /*
         }
         fingerprinters.forEach(function(fingerprinter){
             var tr = document.createElement("tr");
             var td1 = document.createElement("td");
             var td2 = document.createElement("td");
             var td3 = document.createElement("td");
             var td4 = document.createElement("td");
             var a = document.createElement("a");
             a.setAttribute("href", fingerprinter.webPage);
             a.text = fingerprinter.webPage;
             // We add a listener on a link to open the link since it is not possible to do it directly with href attribute
             a.addEventListener("click", openLink);
             var sessionText = document.createTextNode(fingerprinter.sessionId);
             var numberAccessText = document.createTextNode(fingerprinter.attributesAccessed.length);

             var details = document.createElement("a");
             details.setAttribute("href", "#"+fingerprinter.executionId+"---"+fingerprinter.webPage);
             details.text = "details";
             details.setAttribute("class", "details");
             details.addEventListener("click", openDetails);

             td1.appendChild(sessionText);
             td2.appendChild(a);
             td3.appendChild(numberAccessText);
             td4.appendChild(details);
             tr.appendChild(td1);
             tr.appendChild(td2);
             tr.appendChild(td3);
             tr.appendChild(td4);
             fingerprintersTable.appendChild(tr);
         });
       });
*/
}, false);

